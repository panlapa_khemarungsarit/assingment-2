from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'word.views.login_page', name='login_page'),
    url(r'^login/$', 'word.views.login', name='login'),
    url(r'^register/$', 'word.views.register', name='register'),
    url(r'^add/$', 'word.views.add_word', name='add'),
    url(r'^show/$', 'word.views.show_word', name='show'),
    url(r'serch/(\w+)/$', 'word.views.serch', name='serch'),
    url(r'game/$', 'word.views.game', name='game'),
    url(r'^logout/$', 'word.views.logout', name='logout'),
    url(r'^sentence/(\w+)/$', 'word.views.sentence_word', name='sentence'),
)
