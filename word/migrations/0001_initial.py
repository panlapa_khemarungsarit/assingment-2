# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('username', models.TextField(default='')),
                ('password', models.TextField(default='')),
                ('birthday', models.TextField(default='')),
                ('email', models.TextField(default='')),
            ],
        ),
        migrations.CreateModel(
            name='Vocab',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('word', models.TextField(default='')),
                ('mean', models.TextField(default='')),
                ('example', models.TextField(default='')),
                ('url', models.TextField(default='')),
                ('add_date', models.TextField(default='')),
            ],
        ),
    ]
