# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('word', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sentence',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('example', models.TextField(default='')),
                ('url', models.TextField(default='')),
            ],
        ),
        migrations.RemoveField(
            model_name='vocab',
            name='example',
        ),
        migrations.RemoveField(
            model_name='vocab',
            name='url',
        ),
        migrations.AddField(
            model_name='sentence',
            name='vocab',
            field=models.ForeignKey(to='word.Vocab'),
        ),
    ]
