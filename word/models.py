from django.db import models

class Vocab(models.Model):
    word = models.TextField(default='')
    mean = models.TextField(default='')
    add_date = models.TextField(default='')
    # เพิ่ม url, add_date

class Sentence(models.Model):
    vocab = models.ForeignKey(Vocab)
    example = models.TextField(default='')
    url = models.TextField(default='')

class User(models.Model):
    username = models.TextField(default='')
    password = models.TextField(default='')
    birthday = models.TextField(default='')
    email = models.TextField(default='')

