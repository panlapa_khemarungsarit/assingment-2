from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from word.views import add_word
from word.views import show_word
from django.template.loader import render_to_string
from word.models import Vocab

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')  
        self.assertEqual(found.func, add_word) 
   

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = add_word(request)
        expected_html = render_to_string('add_word.html')
        #self.assertEqual(response.content.decode(), expected_html)
   
    
    def test_home_page_displays_all_list_items(self):
        Vocab.objects.create(word='ant' ,mean = 'มด', example = 'ants are small' ,url = 'www.orkin.com/ants/little-black-ants/')

        request = HttpRequest()
        response = show_word(request)       

        self.assertIn('ant', response.content.decode())



    def test_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['word_text'] = 'ant'
        request.POST['mean_text'] = 'มด'
        request.POST['example_text'] = 'ants are small'
        request.POST['url_text'] = 'www.orkin.com/ants/little-black-ants/'
        request.POST['send'] = 'send'

        response = add_word(request)

        self.assertEqual(Vocab.objects.count(), 1)

    def test_home_page_redirects_after_POST(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['word_text'] = 'ant'
        request.POST['mean_text'] = 'มด'
        request.POST['example_text'] = 'ants are small'
        request.POST['url_text'] = 'www.orkin.com/ants/little-black-ants/'
        request.POST['send'] = 'send'

        response = add_word(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/show')


    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        add_word(request)
        self.assertEqual(Vocab.objects.count(), 0)
        
class moneyModelTest(TestCase):


    def test_saving_and_retrieving_items(self):
        first_word = Vocab()
        first_word.word = 'ant'
        first_word.save()

        second_word = Vocab()
        second_word.word = 'bee'
        second_word.save()

        saved_word = Vocab.objects.all()
        self.assertEqual(saved_word.count(), 2)

        first_saved_word = saved_word[0]
        second_saved_word = saved_word[1]
        self.assertEqual(first_saved_word.word,'ant')
        self.assertEqual(second_saved_word.word,'bee')
