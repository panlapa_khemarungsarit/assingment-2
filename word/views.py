#!/usr/bin/python
from django.shortcuts import redirect, render
from word.models import Vocab, User, Sentence
from random import randint
from datetime import date
from django.http import HttpResponse
import operator
count, width, height = 15, 20, 14

class wordplay:
    blue = 0
    def __init__(self, ch, posX, posY, isAns):
        self.ch = ch
        self.posX = posX
        self.posY = posY
        self.isAns = isAns

    def setblue(self):
        self.blue = 1

def add_list(word):
    list_word = []
    list_word_row = []
    for w in word:
        list_word.append(w.word)
        list_word.append(w.mean)
        list_word.append(w.word)
        list_word_row.append(list_word)
        list_word = []
    return sorted(list_word_row)

def login_page(request):
    if request.session.get('user_session', '') != '':
        return redirect('/add')
    return render(request, 'login.html')

def show_word(request):
    if request.session.get('user_session', '') == '':
        return render(
                      request,
                      'link_text.html',
                      {'title_text':"not member",
                       'text1':"You are not member",
                       'data_send':"",
                       'text2':"",
                       'link_text':"go back"
                       })
    if request.method == 'POST' and request.POST.get('sentence','') == 'sentence':
        word = request.POST['id_sentence']
        return redirect('/sentence/%s/' %word)
    words = Vocab.objects.all()
    list_ = add_list(words)
    return render(request, 'show_word.html', {'words': list_ ,'user': request.session['user_session']})

def add_word(request):
    if request.session.get('user_session', '') == '':
        return render(
                      request,
                      'link_text.html',
                      {'title_text':"not member",
                       'text1':"You are not member",
                       'data_send':"",
                       'text2':"",
                       'link_text':"go back"
                       })
    if request.method == 'POST' and request.POST.get('serch','') == 'serch':
        word_serch = request.POST['word_serch']
        if(word_serch == ''):
            return redirect('/serch/null')
        return redirect('/serch/%s/' %word_serch)
    if request.method == 'POST' and request.POST.get('send','') == 'send':
        word_text = request.POST['word_text']
        mean_text = request.POST['mean_text']
        words = Vocab.objects.all()
        for w_c in words :
            if(w_c.word == word_text):
                    return render(request, 'add_word.html', {'text':"this word have in table", 'user': request.session['user_session']})
        Vocab.objects.create(
            word = word_text,
            mean = mean_text,
            )
        return redirect('/show')
    return render(request, 'add_word.html', {'text' : "", 'user': request.session['user_session']})

def serch(request , word_find):
    if request.session.get('user_session', '') == '':
        return render(
                      request,
                      'link_text.html',
                      {'title_text':"not member",
                       'text1':"You are not member",
                       'data_send':"",
                       'text2':"",
                       'link_text':"go back"
                       })
    if request.method == 'POST' and request.POST.get('sentence','') == 'sentence':
        word = request.POST['id_sentence']
        return redirect('/sentence/%s/' %word)
    words = Vocab.objects.all()
    list_word =[]
    list_word_row = []
    for w in words:
        if(word_find in w.word):
            list_word.append(w.word)
            list_word.append(w.mean)
            list_word.append(w.word)
            list_word_row.append(list_word)
            list_word = []
    list_=sorted(list_word_row)
    return render(request, 'show_word.html', {'words':list_,'user': request.session['user_session']})

def sentence_word(request , word_send):
    if request.session.get('user_session', '') == '':
        return render(
                      request,
                      'link_text.html',
                      {'title_text':"not member",
                       'text1':"You are not member",
                       'data_send':"",
                       'text2':"",
                       'link_text':"go back"
                       })
    vocab_ = Vocab.objects.get(word=word_send)
    if request.method == 'POST' and request.POST.get('send','') == 'send':
        example_text = request.POST['example_text']
        url_text = request.POST['url_text']
        Sentence.objects.create(
            vocab = vocab_,
            example = example_text,
            url = url_text,
            )
        return redirect('/sentence/%s/' %word_send)
    return render(request, 'sentence.html', {'vocab':vocab_,'word':vocab_.word,'mean':vocab_.mean,'user': request.session['user_session']})

def game(request):
    if request.session.get('user_session', '') == '':
        return render(
                      request,
                      'link_text.html',
                      {'title_text':"not member",
                       'text1':"You are not member",
                       'data_send':"",
                       'text2':"",
                       'link_text':"go back"
                       })
    if request.method == "POST":
        rows = request.POST.get('platform', '')
        platform = [[wordplay(char.split(',')[0], int(char.split(',')[1]), int(char.split(',')[2]), int(char.split(',')[3])) for char in row.split(')') if char != ''] for row in rows.split('/') if row != '']
        queststr, finish = request.POST['quest'], 'no'
        meanstr = request.POST['mean']
        quest = [i for i in queststr.split(',') if i != '']
        mean = [i for i in meanstr.split(',') if i != '']
        type = request.POST['type']
        if request.POST["val"] == '': type = 'reset'
        if type == 'submit':
            if request.POST["val"] in quest:
                for q in range(len(quest)):
                    if quest[q] == request.POST["val"]:
                        if len(quest) == 1:
                            finish = 'yes'
                        quest.pop(q)
                        mean.pop(q)
                        break
                for row in request.POST['position'].split(','):
                    if row != '':
                        pos = row.split('_')
                        platform[int(pos[1])][int(pos[0])].isAns = 2
            print(quest)
            ###increase score###
            return render(request, 'wordhunt.html', {
                   'rows':platform,
                   'cheat':request.POST['cheat'],
                   'user':request.session.get('user_session', ''),
                   'fin':finish,
                   'quest':quest,
                   'mean':mean,
            })
        elif type == 'reset':
            select = ''
            try:
                platform[int(request.POST['val'].split('_')[1])][int(request.POST['val'].split('_')[0])].setblue()
                select = (request.POST['val'] == '') and '' or platform[int(request.POST['val'].split('_')[1])][int(request.POST['val'].split('_')[0])]
            except: pass
            return render(request, 'wordhunt.html', {
                   'rows':platform,
                   'selected':select,
                   'cheat':(request.POST['cheat'] == 'yes') and 'yes' or 'no',
                   'user':request.session.get('user_session', ''),
                   'quest':quest,
                   'mean':mean,
               })
    if Vocab.objects.count() >= count:
        words, means, choose, mean = [""]*Vocab.objects.count(), [""]*Vocab.objects.count(), [""]*count, [""]*count
        k = 0
        for word in Vocab.objects.all():
            words[k] = word.word
            means[k] = word.mean
            k += 1
        k = 0
        for i in range(count):
            while k < count:
                try:
                    ran = randint(0, Vocab.objects.count())
                    if not words[ran] in choose:
                        choose[k] = words[ran]
                        mean[k] = means[ran]
                        k += 1
                except:pass
        while 1:
            success = 0
            chars = [['' for j in range(width)] for i in range(height)]
            for i in range(count):
                for z in range(30):
                    try:
                        dx = randint(-1, 1)
                        dy = (dx == 0) and (randint(0, 1)*2)-1 or randint(-1, 1)
                        startx = randint(0, width-1)
                        starty = randint(0, height-1)
                        avail = 1
                        for j in range(len(choose[i])):
                            if chars[starty + (dy*j)][startx + (dx*j)] != '' or\
                               startx + (dx*j) >=width or\
                               startx + (dx*j) < 0 or\
                               starty + (dy*j) >= height or\
                               starty + (dy*j) < 0:
                                avail = 0
                        if avail:
                            for j in range(len(choose[i])):
                                chars[starty + (dy*j)][startx + (dx*j)] = choose[i][j]
                            success += 1
                            break
                    except: pass
            if success == count:break
            
        disp = [[(chars[j][i] == '') and wordplay(chr(randint(0, 25) + 97),i, j, 0) or wordplay(chars[j][i], i, j, 1) for i in range(width)] for j in range(height)]
        return render(request, 'wordhunt.html', {
                   'rows':disp,
                   'user':request.session.get('user_session', ''),
                   'quest':choose,
                   'mean':mean,
               })
                        
    return render(request, 'wordhunt.html', {
        'error':"less15",
        'user':request.session.get('user_session', ''),
    })

def register(request):
    fail = ''
    if request.session.get('user_session', '') == '' and request.method == 'POST':
        tokens = [request.POST[i] for i in ['user', 'pass', 'birth', 'email']]
        print(tokens)
        try:
            username = User.objects.get(username=tokens[0])
            fail = 'user'
        except:
            User.objects.create(username=tokens[0], password=tokens[1], birthday=tokens[2], email=tokens[3])
            request.session['user_session'] = tokens[0]
            return render(
                      request,
                      'link_text.html',
                      {'title_text':"Success",
                       'text1':"Register success",
                       'data_send':tokens[0],
                       'text2':"you are now registered ",
                       'link_text':"continue"
                       })
        return render(request, 'register.html', {
            'fail':fail,
        })
    elif request.session.get('user_session', '') == '':
        return render(request, 'register.html')
    return render(
                      request,
                      'link_text.html',
                      {'title_text':"You are now loggin",
                       'text1':"You are login as",
                       'data_send':request.session['user_session'],
                       'text2':"",
                       'link_text':"continue"
                       })

def login(request):
    if request.method == 'POST':
        try:
            user = User.objects.get(username=request.POST['user'])
            if request.POST['pass'] == user.password:
                request.session['user_session'] = user.username
                return render(
                      request,
                      'link_text.html',
                      {'title_text':"login success",
                       'text1':"Hello",
                       'data_send':user.username,
                       'text2':"you are now login",
                       'link_text':"continue"
                       })
            return render(
                      request,
                      'link_text.html',
                      {'title_text':"password not match",
                       'text1':"Password does not match",
                       'data_send':"",
                       'text2':"",
                       'link_text':"go back"
                       })
        except:
            return render(
                      request,
                      'link_text.html',
                      {'title_text':"no user match",
                       'text1':"no user match this user",
                       'data_send':"",
                       'text2':"",
                       'link_text':"go back"
                       })
    return redirect('/')

def logout(request):
    request.session['user_session'] = ''
    return render(
                      request,
                      'link_text.html',
                      {'title_text':"logout",
                       'text1':"You are now logout",
                       'data_send':"",
                       'text2':"",
                       'link_text':"go back"
                       })
