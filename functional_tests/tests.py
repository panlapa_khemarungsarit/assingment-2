from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time

class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_answer(self, word, table):
        print(word)
        print(str(len(table))+str(len(table[0])))
        for j in range(len(table)):
            for i in range(len(table[0])):
                if word[0] is table[j][i]:
                    for k in [-1, 0, 1]:
                        for l in [-1, 0, 1]:
                            if not (k == 0 and l == 0) :
                                s = table[j][i]
                                print(str(k) + str(l))
                                while s == word[:len(s)]:
                                    if s == word:
                                        return [i, j, k, l, len(s)]
                                    if i+(k*len(s)) >= len(table[0]) or i+(k*len(s)) < 0 or j+(l*len(s)) >= len(table) or j+(l*len(s)) < 0:
                                        break
                                    print(s + "," + word[:len(s)] + "," + table[j+(l*len(s))][i+(k*len(s))])
                                    s += table[j+(l*len(s))][i+(k*len(s))]
        return 'fail'

    def test_word(self):
        self.check1_register_and_login()
        self.check_no_word()
        self.check2_send_word()
        # self.check3_serch()
        self.play_game_word_hunt()
        

    def check1_register_and_login(self):
        self.browser.get(self.live_server_url)
        self.browser.find_element_by_id('register').click()
        time.sleep(1)
        self.browser.find_element_by_id('user').send_keys('batman')
        self.browser.find_element_by_id('pass').send_keys('toppreawbook')
        self.browser.find_element_by_id('birth').send_keys('01-01-1970')
        self.browser.find_element_by_id('email').send_keys('iAmBatMan@batmail.com')
        self.browser.find_element_by_id('send').click()
        time.sleep(2)
        

    def check2_send_word(self):
        self.browser.get(self.live_server_url)
        word_test = ['ant', 'bee', 'data', 'future', 'elephant', 'balloon', 'chair', 'table', 'magic', 'black', 'white', 'coin', 'tomorrow', 'yesterday', 'past']
        mean_test = ['มด', 'ผึ้ง', 'ข้อมูล', 'อนาคต', 'ช้าง', 'บอลลูน', 'เก้าอี้', 'โต๊ะ', 'เวทย์มนต์', 'ดำ', 'ขาว', 'เหรียญ', 'พรุ่งนี้', 'เมื่อวาน', 'อดีต']
        for i in range(15):
            self.browser.find_element_by_id('word_new').send_keys(word_test[i])
            self.browser.find_element_by_id('mean_new').send_keys(mean_test[i])
            self.browser.find_element_by_id('button').click()
            self.browser.find_element_by_id('add').click()

    def check3_serch(self):
        self.browser.get(self.live_server_url)
        inputbox_word = self.browser.find_element_by_id('word_serch_new')
        inputbox_word.send_keys('ant')
        button = self.browser.find_element_by_id('button_serch')
        button.click()
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn('ant มด', [row.text for row in rows])
        self.assertIn('ตัวอย่างประโยค ants are small', [row.text for row in rows])
        self.assertIn('แหล่งที่มา http://www.orkin.com/ants/little-black-ants/', [row.text for row in rows])

    def check_no_word(self):
        self.browser.get(self.live_server_url+'/game')
        self.assertTrue(any((i.get_attribute('innerHTML') == 'your words are less than 15 please add words up to 15 or more') for i in self.browser.find_elements_by_tag_name('h1')))


    def play_game_word_hunt(self):
        self.browser.get(self.live_server_url+'/game')

        #Test title#
        self.assertEquals("Word Hunt !!", self.browser.title)
        time.sleep(1)

        questwords = self.browser.find_element_by_id('quest').get_attribute('value').split(',')[:-1]
        area = [[self.browser.find_element_by_id('gametable').find_element_by_id('b_'+str(i)+'_'+str(j)).get_attribute('value') for i in range(20)] for j in range(14)]
        for word in questwords:
            duty = self.check_for_answer(word, area)
            print(duty)
            if duty == 'fail':
                self.fail("Error : can't find answer")
            for i in range(duty[4]):
                self.browser.find_element_by_id('gametable').find_element_by_id('b_'+str(duty[0]+(duty[2]*i))+'_'+str(duty[1]+(duty[3]*i))).click()
            self.browser.find_element_by_id('send').click()
        
        time.sleep(2)



if __name__ == '__main__':
    unittest.main(warnings='ignore')
